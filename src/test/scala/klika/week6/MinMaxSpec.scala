package klika.week6

import org.scalatest._

class MinMaxSpec extends FlatSpec with Matchers {
  "MinMax" should "return pair of min and max" in {
    val xs = (1 to 100).toArray

    assert(MinMax(xs) == (1, 100))
  }

  "MinMax" should "work even with shuffled array" in {
    val xs = util.Random.shuffle(10 to 50).toArray

    assert(MinMax(xs) == (10, 50))
  }

  "MinMax" should "work with negative values" in {
    val xs = util.Random.shuffle(-100 to 100).toArray

    assert(MinMax(xs) == (-100, 100))
  }
}
