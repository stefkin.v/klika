package klika.week6

import com.twitter.util.Return
import com.twitter.finagle.service._
import com.twitter.finagle.http.service.HttpResponseClassifier
import org.scalatest._
import org.scalatest.concurrent._
import org.scalamock.proxy.ProxyMockFactory
import scala.language.postfixOps
import com.twitter.util.{Await}
import com.twitter.conversions.time._
import org.scalamock.scalatest.MockFactory
import com.twitter.finagle.http._
import org.scalamock.proxy.ProxyMockFactory
import io.finch._
import com.twitter.finagle.Http
import scala.io.Source
import com.twitter.io.{Reader, Buf}
import com.twitter.finagle.ListeningServer

class RandomUserSpec extends BaseSpec {

  val resource: (String => String) = (file)=> {
    val stream = getClass.getResourceAsStream(s"/$file")
    Source.fromInputStream(stream).mkString
  }

  val singleUser: Endpoint[String] = get("api") {
    Ok(resource("singleUser.json"))
  }

  var server: ListeningServer = _
  val ru = RandomUser("localhost:1490", "http")

  after {
    Await.ready(server.close())
  }

  it should "fetch single user successfully" in {
    server = Http.server.serve(":1490", singleUser.toServiceAs[Text.Plain])

    val user = Await.result(ru.fetchBatch).head
    user.name should equal("judith matthews")
    user.gender should equal("female")
    user.location should equal("dunboyne, 5877 green lane")
    user.email should equal("judith.matthews@example.com")
    user.phone should equal("041-504-7801")
  }

  it should "handle failure gracefully" in {
    val user = ru.extractUsers("")

    user should equal(Nil)
  }

  val fiveUsers: Endpoint[String] = get("api") {
    Ok(resource("fiveUsers.json"))
  }

  it should "fetch a ton of users at a time" in {
    server = Http.server.serve(":1490", fiveUsers.toServiceAs[Text.Plain])

    val users = Await.result(ru.fetchBatch)

    assert(users.size == 5)

  }

  var attempts = 0
  val failure: Endpoint[String] = get("api") {
    if (attempts > 1) {
      Ok(resource("singleUser.json"))
    }
    else {
      attempts += 1
      BadGateway(new Exception("try again"))
    }
  }

  it should "avoid network issues by retrying" in {
    // ClientBuilder.retries doesn't work for some reason
    // Using crappy self-implemented retry :(

    server = Http.server
      .withResponseClassifier(ru.classifier)
      .serve(":1490", failure.toServiceAs[Text.Plain])

    val users = Await.result(ru.fetchBatch)
    assert(users.length > 0)
  }
}
