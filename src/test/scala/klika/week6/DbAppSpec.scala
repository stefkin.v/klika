package klika.week6

import slick.driver.PostgresDriver.api._
import slick.backend.DatabasePublisher

class DbAppSpec extends BaseSpec {
  override def beforeAll = {
    UserRepository.db.run(sqlu"""drop table if exists "USERS";""")
    UserRepository.create
    ApiApp().boot()
  }

  it should "load users in db using api app" in {
    DbApp().run

    whenReady(UserRepository.count) { count =>
      assert(count == 1000)
    }
  }
}
