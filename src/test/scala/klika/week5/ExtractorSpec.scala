package klika.week5
import org.scalatest._

class ExtractorSpec extends FlatSpec with Matchers {

  def extract(str: String): String = str match {
    case HexExtractor(hex) => s"Hex number: $hex"
    case EmailExtractor(email) => s"Email: $email"
    case UrlExtractor(_, domain, port, _) => s"Domain: $domain Port: $port"
    case _ => ""
  }

  "Hex numbers" should "be extracted" in {
    assert(extract("00AF") == "Hex number: 00AF")
  }


  "Emails" should "be extracted" in {
    assert(extract("foo@example.com") == "Email: foo@example.com")
  }

  "URLs" should "be extracted" in {
    assert(extract("https://example.com:8080/dashboard") == "Domain: example.com Port: 8080")
  }

  "Default port for https" should "be set" in {
    assert(extract("https://example.com") == "Domain: example.com Port: 443")
  }

  "Default port for http" should "be also set" in {
    assert(extract("http://example.com/dashboard") == "Domain: example.com Port: 80")
  }
}
