package klika.week5

import org.scalatest._

class TreeSpec extends FlatSpec with Matchers {
  trait TestTrees {
    val t1 = new NonEmpty[Int](5, Empty, Empty)
      .insert(1)
      .insert(1)
      .insert(2)
      .insert(6)
      .insert(7)

    val e = Empty
  }

  "to list of empty" should "be empty" in {
    new TestTrees {
      assert(e.toList == Nil)
      assert(e.toList.size == 0)
    }
  }

  "to list of non empty" should "be of correct size" in {
    new TestTrees {
      assert(t1.toList.size == 6)
    }
  }

  "map of empty" should "be empty" in {
    new TestTrees {
      assert(e.map((x)=> x).toList.size == 0)
    }
  }

  "map of non empty" should "be the same size" in {
    new TestTrees {
      assert(t1.map(_ * 3).toList.size == 6)
    }
  }

  "map of non empty" should "return tree with diff elements" in {
    new TestTrees {
      assert(t1.map(_ * 3).toList.contains(3))
      assert(t1.map(_ * 3).toList.contains(6))
      assert(t1.map(_ * 3).toList.contains(15))
      assert(t1.map(_ * 3).toList.contains(18))
      assert(t1.map(_ * 3).toList.contains(21))
    }
  }

  "reduce of empty" should "throw error" in {
    new TestTrees {
      an [NoSuchElementException] should be thrownBy e.reduce((x: Int, y: Int) => y)
    }
  }

  "reduce of non empty" should "return correct result" in {
    new TestTrees {
      assert(t1.reduce((x,y) => x) == 5)
      assert(t1.reduce(_ + _) == 22)
    }
  }
}
