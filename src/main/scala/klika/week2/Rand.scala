package klika.week2

case class Rand(seed: Long = System.currentTimeMillis) {
  def nextInt: (Int, Rand) = (int, nextRand)

  def newSeed: Long = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
  // Dark sorcery from "FP in Scala" p. 81

  def nextRand = Rand(newSeed)

  def int = (newSeed >>> 16).toInt
}
