package klika.week2
import scala.util.parsing.combinator.JavaTokenParsers

object Calc extends JavaTokenParsers {
  def eval(str: String): ParseResult[D] = parseAll(expr, str)

  def expr: Parser[D] = term ~ rep(add | sub) ^^ {
    case number ~ list => list.foldLeft(number)((acc, f) => f(acc))
  }

  def add: Parser[D => D] = "+" ~ term ^^ { case "+" ~ b => _ + b }

  def sub: Parser[D => D] = "-" ~ term ^^ { case "-" ~ b => _ - b }

  def term: Parser[D] = factor ~ rep(mul | div) ^^ {
    case number ~ list => list.foldLeft(number)((acc, f) => f(acc))
  }

  def mul: Parser[D => D] = "*" ~ factor ^^ { case "*" ~ b => _ * b }

  def div: Parser[D => D] = "/" ~ factor ^^ { case "/" ~ b => _ / b }

  def factor: Parser[D] = num | "(" ~> expr <~ ")"

  def num: Parser[D] = floatingPointNumber ^^ (str => D(str.toDouble))
}
