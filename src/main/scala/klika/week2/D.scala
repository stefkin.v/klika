package klika.week2

case class D(d: Double) {
  def +(that: D) = new D(this.d + that.d)
  def -(that: D) = new D(this.d - that.d)
  def *(that: D) = new D(this.d * that.d)
  def /(that: D) = new D(this.d / that.d)

  def +:(that: D) = new D(this.d + that.d) // same associativity
  def *:(that: D) = new D(this.d * that.d) // different priority
  def -:(that: D) = new D(this.d - that.d)
  def /:(that: D) = new D(this.d / that.d)

  def :-(that: D) = new D(this.d - that.d)
  def :-:(that: D) = new D(this.d - that.d)
}
