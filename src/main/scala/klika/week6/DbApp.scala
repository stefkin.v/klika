package klika.week6
import com.twitter.finagle.Http
import com.twitter.finagle.http.RequestBuilder
import com.twitter.util.Await
import io.circe._, io.circe.generic.auto._, io.circe.parser._

final class DbApp {
  lazy val client = Http.client.newService("localhost:8888")

  def requestUsers = RequestBuilder()
    .url(s"http://localhost:8888/v0/users")
    .buildGet()

  def run = {
    val response: String = Await.result(client(requestUsers)).contentString
    val users = decode[Seq[User]](response).getOrElse(Nil)
    UserRepository.insertBatch(users)
  }
}

object DbApp {
  def apply() = new DbApp()
}
