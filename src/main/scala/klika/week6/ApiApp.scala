package klika.week6

import com.twitter.finagle.Http
import com.twitter.util.Await

final class ApiApp {
  lazy val ru = RandomUser()

  lazy val server = Http.server
    .serve(":8888", ru.getUsers)

  def boot(): Unit = {
    sys.addShutdownHook(shutdown())
    server
  }

  def shutdown(): Unit = Await.ready(server.close())
}

object ApiApp {
  def apply() = new ApiApp()
}
