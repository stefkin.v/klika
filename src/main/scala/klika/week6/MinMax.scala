package klika.week6

object MinMax {
  def apply(xs: Array[Int]): (Int, Int) =
    (xs.reduce(_ min _), xs.reduce(_ max _))
}
