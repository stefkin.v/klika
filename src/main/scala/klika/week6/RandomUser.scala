package klika.week6

import java.net.URL
import io.circe._, io.circe.generic.auto._, io.circe.jawn._, io.circe.syntax._
import com.twitter.finagle.http._
import com.twitter.finagle.Http
import com.twitter.finagle.Service
import com.twitter.finagle.Codec
import scala.concurrent.ExecutionContext.Implicits.global
import com.twitter.conversions.time._
import com.twitter.finagle.http.service.HttpResponseClassifier
import com.twitter.finagle.service._
import com.twitter.util.Return
import io.finch._
import io.finch.circe._
import io.circe.generic.auto._
import com.twitter.util._
import com.twitter.finagle.param.HighResTimer

class RandomUser(val host: String = "randomuser.me", val protocol: String = "https") {

  def getUsers  = {
    io.finch.get("v0" :: "users") {
      fetchBatch.map { io.finch.Ok(_) }
    }.toService
  }

  val budget: RetryBudget = RetryBudget(
    ttl = 10.seconds,
    minRetriesPerSec = 5,
    percentCanRetry = 0.1
  )

  val classifier: ResponseClassifier = {
    case ReqRep(_, Return(r: Response)) if r.statusCode == 200 => ResponseClass.Success
    case _ => ResponseClass.RetryableFailure
  }

  def client: Service[Request, Response] = {
    protocol match {
      case "https" => Http.client.withTls(host).newService(s"$host:443")
      case _ => Http.client.newService(host)
    }
  }

  val requestUsers: (Int => Request) = (n) => RequestBuilder()
    .url(s"$protocol://$host/api?results=$n")
    .buildGet()

  val decodeUser: (Cursor => Option[User]) = (cursor)=> {
    for {
      name <- cursor.downField("name")
      first <- name.downField("first")
      last <- name.downField("last")

      gen <- cursor.downField("gender")

      email <- cursor.downField("email")

      location <- cursor.downField("location")

      city <- location.downField("city")
      street <- location.downField("street")

      phone <- cursor.downField("phone")
    } yield User(
      s"${first.as[String].getOrElse("")} ${last.as[String].getOrElse("")}",
      gen.as[String].getOrElse(""),
      s"${city.as[String].getOrElse("")}, ${street.as[String].getOrElse("")}",
      email.as[String].getOrElse(""),
      phone.as[String].getOrElse("")
    )
  }

  val extractUsers: (String => Seq[User]) = (str) => {
    val userCursor: Option[Cursor] = for {
      results <- parse(str).getOrElse(Json.Null).cursor.downField("results")
      userCursor <- results.downArray
    } yield userCursor

    def loop(maybeCursor: Option[Cursor]): Seq[User] =
      maybeCursor.flatMap(decodeUser) match {
        case Some(user) => user +: loop(maybeCursor.flatMap(_.right))
        case _ => Nil
      }

    loop(userCursor)
  }

  import com.twitter.util.{Future => TFuture, Await => TAwait}

  def fetchBatch: TFuture[Seq[User]] = {
    retry.andThen(client)(requestUsers(1000))
      .map(_.contentString)
      .map(extractUsers)
  }

  val shouldRetry: PartialFunction [(Request, Try[Response]), Boolean] = {
    case (_, Return(rep)) => rep.status != 200
  }

  implicit val t: Timer = HighResTimer.Default
  val retry = RetryFilter(Backoff.const(1.second).take(5))(shouldRetry)

}

object RandomUser {
  def apply(host: String, protocol: String) = new RandomUser(host, protocol)
  def apply() = new RandomUser()
}
