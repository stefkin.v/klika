package klika.week6

object PhoneParser {
  val pattern = """.*, (\d+)-(\d+)-(\d+),.*""".r

  def apply(str: String): (Int, Int, Int) = {
    val pattern(one, two, three) = str
    (one.toInt, two.toInt, three.toInt)
  }
}
