package klika.week5
import scala.annotation.tailrec

object Factorial {

  val zero: BigInt = 0

  @tailrec
  def apply(n: BigInt, acc: BigInt = 1): BigInt = n match {
    case `zero` => acc
    case _ => apply(n - 1, n * acc)
  }
}
