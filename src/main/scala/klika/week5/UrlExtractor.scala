package klika.week5
import scala.util.matching.Regex

object UrlExtractor {
  val Pattern = """(https?)\:\/\/([\w\.]+)\:?(\d+)?(\/\w+)?""".r

  def unapply(str: String): Option[(String, String, Int, String)] = str match {
    case Pattern(protocol, domain, null, path) =>
      Some((protocol, domain, portFor(protocol), path))
    case Pattern(protocol, domain, port, path) =>
      Some((protocol, domain, port.toInt, path))
    case _ => None
  }

  def portFor(protocol: String) = protocol match {
    case "https" => 443
    case _ => 80
  }
}
